# rebootr

`rebootr` is a BASH script that enumerates your current UEFI boot entries and allows you to select which to boot into on your next reboot. This is a "boot once" selection and DOES NOT affect your default selection on subsequent reboots.

No more needing to catch your BIOS/UEFI menu or messing with GRUB.

## Instructions

To use the script, download it and change the file permissions to make it executable then run it.

To use git to clone:

    git clone https://gitlab.com/sleepyeyesvince/rebootr.git

To change file permission to make it executable:

    chmod +x rebootr

To run the script:

    ./rebootr

## Additional Info

Tip:
This script works remotely over SSH :) 
